# Readme

- Clone this repository
- Go to `chrome://extensions` and load this extension.
- Now the text will not be obfuscated when you visit 
- Open `src/index.html ` in a browser to view the  [protext](http://protext.hackerrank.com) site
- This code sends the fonts and the obfuscated text to backend and gets the original text back
- view the backend code [here](https://bitbucket.org/tanmaydatta/protext_break_backend)